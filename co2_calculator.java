import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class co2_calculator {

    public static void main(String[] args) throws ParseException {
        // TODO Auto-generated method stub

        try {
            co2_calculator coc = new co2_calculator();

            Options options = new Options();

            //creating Argument as options 
            Option transportation = Option.builder()
                .longOpt("transportation-method")
                .argName("String")
                .required(false)
                .hasArg().optionalArg(true)
                .desc("Valid Transportation Method")
                .build();
            options.addOption(transportation);

            Option distance = Option.builder()
                .longOpt("distance")
                .argName("Floar")
                .required(false)
                .hasArg().optionalArg(true)
                .desc("Enter Distance")
                .build();
            options.addOption(distance);


            Option unit0fdistance = Option.builder()
                .longOpt("unit-of-distance")
                .argName("String")
                .hasArg().optionalArg(true)
                .required(false)
                .desc("Valid Transportation Method")
                .build();
            options.addOption(unit0fdistance);

            Option output = Option.builder()
                .longOpt("output")
                .argName("String")
                .hasArg().optionalArg(true)
                .required(false)
                .desc("Enter Required Output fornat eiter in kg - kilo gram or g-grams Default is g")
                .build();
            options.addOption(output);

            Option helpOption = Option.builder("h")
                .longOpt("help")
                .required(false)
                .hasArg(false)
                .build();
            options.addOption(helpOption);



            CommandLineParser parser = new DefaultParser();
            //Parsing Input to options 
            CommandLine cmd = parser.parse(options, args);
            //Generating Help
            // HelpFormatter formatter = new HelpFormatter();
            // formatter.printHelp("help", options, true);

            //Creating Hashmap to store the Transportation methods in CO2e per passenger per km:

            Boolean err = false;
            float dist = 0;
            String transport = null;
            String outp;
            String uod;

            Map < String, Integer > transportationmethos = new HashMap < String, Integer > ();

            transportationmethos.put("small-diesel-car", 142);
            transportationmethos.put("small-petrol-car", 154);
            transportationmethos.put("small-plugin-hybrid-car", 73);
            transportationmethos.put("small-electric-car", 50);
            transportationmethos.put("medium-diesel-car", 171);
            transportationmethos.put("medium-petrol-car", 192);
            transportationmethos.put("medium-plugin-hybrid-car", 110);
            transportationmethos.put("medium-electric-car", 58);
            transportationmethos.put("large-diesel-car", 209);
            transportationmethos.put("large-petrol-car", 282);
            transportationmethos.put("large-plugin-hybrid-car", 126);
            transportationmethos.put("large-electric-car", 73);
            transportationmethos.put("bus", 27);
            transportationmethos.put("train", 6);




            if (cmd.hasOption("transportation-method")) {
                transport = cmd.getOptionValue("transportation-method");
            } else {
                err = true;
            }

            if (cmd.hasOption("distance")) {
                dist = Float.parseFloat(cmd.getOptionValue("distance"));
            } else {
                err = true;
            }


            if (cmd.hasOption("help") || cmd.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("co2_calculator", options, true);
            }


            if (cmd.hasOption("unit-of-distance")) {
                uod = cmd.getOptionValue("unit-of-distance");
            } else {
                uod = "km";
            }


            if (cmd.hasOption("output")) {
                outp = cmd.getOptionValue("output");
            } else {
                outp = "g";
            }

            //geting Distance (converting m to km )

            float distan = 0;

            if (uod.equals("m")) {
                distan = coc.convert(dist);
            } else if (uod.equals("km")) {
                distan = dist;
            } else {
                err = true;
            }

            int avgco2 = transportationmethos.get(transport);


            if (!err) {
                float result = avgco2 * distan;

                if (outp.equals("kg")) {
                    result = coc.convert(result);
                } else if (outp.equals("g")) {
                    //do nothing
                } else {
                    err = true;
                }

                if (!err) {
                    System.out.printf("Your trip caused %.1f" + outp + " of CO2-equivalent.", result);
                } else {
                    coc.printerror();
                }

            } else {
                coc.printerror();
            }

        } catch (Exception e) {

        }

    }

    float convert(float val) {
        float res;
        res = val / 1000;
        return res;

    }

    void printerror() {
        System.out.printf("Invalid arguments use --help or -h for usage information");
    }

}